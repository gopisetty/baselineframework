
package bl.framework.design;

import java.io.IOException;

import org.openqa.selenium.WebElement;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;

import utils.ExtentsReports;
import utils.ReadExcel;

public class ProjectMethods extends ExtentsReports{

	public String testcaseName,testDec,author,category,dataSheetname;
	
	public void beforeSuite()
	{
		startReport();
	}
	
	@BeforeMethod
	
	public void login() {
	startApp("chrome", "http://leaftaps.com/opentaps");
	
	WebElement eleUsername = locateElement("id", "username");
	clearAndType(eleUsername, "DemoSalesManager"); 
	
	WebElement elePassword = locateElement("id", "password");
	clearAndType(elePassword, "crmsfa"); 
	
	WebElement eleLogin = locateElement("class", "decorativeSubmit");
	click(eleLogin); 
	
	WebElement elecrmsfa = locateElement("partiallinktext","CRM/SF");
	click(elecrmsfa);
	
	//Implement @Before Method in Project Methods and run two testcase in parallen and seq
	
	
}
	
@DataProvider(name ="getData1")	
public Object[][] getData() throws IOException
{
	return ReadExcel.readData(dataSheetname);
}



}
