package bl.framework.testcases;

import org.openqa.selenium.WebElement;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import bl.framework.design.ProjectMethods;


public class TC003_CreateLead extends ProjectMethods{
	//@Test(dependsOnMethods= "bl.framework.testcases.TC003_DuplicateLead.DuplicateLead")
	//@Test(invocationCount=2,threadPoolSize=2)
	@BeforeTest
	public void setData() {
		testcaseName = "TC001_CreateLead";
		testDec = "Create a new Lead in leaftaps";
		author = "Gayatri";
		category = "Smoke";
		dataSheetname ="readdata";
	} 
	@Test
	public void createLead() {
		click(locateElement("link", "Leads"));
		click(locateElement("link", "Create Lead"));
		clearAndType(locateElement("id", "createLeadForm_companyName"), "TL");
		clearAndType(locateElement("id", "createLeadForm_firstName"), "koushik");
		clearAndType(locateElement("id", "createLeadForm_lastName"), "c");
		click(locateElement("name", "submitButton")); 
	}
	
	@Test(dataProvider="getData1") //calling data provider using the name
	public void createLead(String cName, String fName, String lName) {
		
	 
		WebElement eleCreateLead = locateElement("linkText","Create Lead");
		click(eleCreateLead);
		
		WebElement comName = locateElement("id", "createLeadForm_companyName");
		clearAndType(comName, cName);
		
		WebElement FName = locateElement("id", "createLeadForm_firstName");
		clearAndType(FName, fName);
		
		WebElement LName = locateElement("id", "createLeadForm_lastName");
		clearAndType(LName, lName);
		
		WebElement CreatLeadButton = locateElement("class", "smallSubmit");
		click(CreatLeadButton);
		
		//String alertText = getAlertText();
		
	}
@DataProvider(name="getData")	// Declaring data provider annotations with specific name	
public String[][] fetchData() {
	
	String[][] data = new String[2][3];
	
	data[0][0]="TCS";
	data[0][1]="Manica";
	data[0][2]= "Devi";
	
	data[1][0]="TCS1";
	data[1][1]="Fname";
	data[1][2]= "Lname";
	
	return data;
}
@DataProvider(name="getData1")	
public String[][] fetchData1() {
	
	String[][] data = new String[2][3];
	
	data[0][0]="ABC";
	data[0][1]="Yasaswini";
	data[0][2]= "G";
	
	data[0][0]="DEF";
	data[0][1]="Fname1";
	data[0][2]= "Lname1";
	
	return data;
}


	}