package bl.framework.testcases;

import org.openqa.selenium.WebElement;
import org.testng.annotations.Test;

import bl.framework.api.SeleniumBase;

public class TC002_DropDown extends SeleniumBase{

	@Test
	public void dropdown()
	{
		startApp("chrome","http://www.leafground.com/pages/Dropdown.html");
		
		WebElement ele = locateElement("id","dropdown1");
		selectDropDownUsingText(ele, "Selenium");
		
		WebElement ele1 = locateElement("name","dropdown2");
		selectDropDownUsingValue(ele1, "2");
	}
	
}
